let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
var enviarTexto = require("../controllers/index");
chai.use(chaiHttp);
const url = 'http://localhost:3001';

describe('Pruebas unitarias con chai: ', () => {
    it('Probar envio y recepcion de texto', (done) => {
        chai.request(url)
            .post('/test')
            .send({ texto: "texto" })
            .end(function(err, res) {
                expect(res).to.have.status(200);
                done();
            });
    });
    it('Probar texto igual al enviado', (done) => {
        chai.request(url)
            .post('/test')
            .send({ texto: "texto" })
            .end(function(err, res) {
                expect(res.text).to.equal("texto");
                done();
            });
    });
    it('Probar texto sea string', (done) => {
        chai.request(url)
            .post('/test')
            .send({ texto: "texto" })
            .end(function(err, res) {
                expect(res.text).to.be.a('string');
                done();
            });
    });
    it('Probar texto mida 5 caracteres', (done) => {
        chai.request(url)
            .post('/test')
            .send({ texto: "texto" })
            .end(function(err, res) {
                expect(res.text).to.have.lengthOf(5);
                done();
            });
    });
});