import axios from 'axios';
import * as formActions from '../actions/formAction';
import { create } from "react-test-renderer";
import React from 'react'
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import reducer from "../reducers/formReducer";
import TextForm from '../components/form';

const middlewares = [thunk];

if (process.env.NODE_ENV === `development`) {
   const { logger } = require(`redux-logger`);

   middlewares.push(logger);
}

const store = createStore(reducer, applyMiddleware(...middlewares));

describe("Form component", () => {
    it("Componente de formulario renderizado: ", () => {
        const component = create(
        <Provider store={store}>
	    	<div>
				<TextForm/>
			</div>W
	    </Provider>);
        expect(component.toJSON()).toMatchSnapshot();
    });
});