import { SEND_TEXT_REQUEST , SEND_TEXT_SUCCESS , SEND_TEXT_FAILURE } from "../types/formType";
import axios from 'axios';

export const setCurrentText = (texto) => {
  return (dispatch) => {
    dispatch(setCurrentTextRequest())
    axios
      .post(`http://localhost:3001/test/`, { "texto": texto })
      .then(response => {
        dispatch(setCurrentTextSuccess(response.data))
      })
      .catch(error => {
        dispatch(setCurrentTextFailure("Fallo de comunicacion intente nuevamente mas tarde"))
      })
  }
}

export const setCurrentTextRequest = () => {
  return {
    type: SEND_TEXT_REQUEST
  }
}

export const setCurrentTextSuccess = texto => {
  return {
    type: SEND_TEXT_SUCCESS,
    payload: texto
  }
}

export const setCurrentTextFailure = error => {
  return {
    type: SEND_TEXT_FAILURE,
    payload: error
  }
}