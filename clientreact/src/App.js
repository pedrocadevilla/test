import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Index } from './pages/index';
import { Acerca } from './pages/acerca';
import { Contacto } from './pages/contacto';
import { NotFound } from './pages/notFound';
import { Layout } from './components/layout';
import { NavigationBar } from './components/navigationBar';
import { Jumbotron } from './components/jumbotron.js'; 

function App() {
    return (
      <React.Fragment>
        <NavigationBar></NavigationBar>
        <Jumbotron></Jumbotron>
        <Layout>
          <Router>  
            <Switch>
              <Route exact path="/" component={Index} />
              <Route path="/acerca" component={Acerca} />
              <Route exact path="/contacto" component={Contacto} />
              <Route component={NotFound} />
            </Switch>
          </Router>
        </Layout>
      </React.Fragment>
    );
}

export default App;