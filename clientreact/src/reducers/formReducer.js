import { SEND_TEXT_REQUEST, SEND_TEXT_SUCCESS, SEND_TEXT_FAILURE } from "../types/formType";

const initialState = {
    textForm: {
        values: {
            texto: ""
        },
        errors: {
            texto: ""
        }
    }
};

const setErrors = (palabra) => {
    let errors = { texto: "" };
    if (!palabra && palabra.length === 0) {
        errors.texto = "Debe introducir un texto";
    } else if (palabra.length <= 3) {
        errors.texto = "El texto debe contener mas de 3 caracteres";
    } else if (typeof palabra !== "string") {
        errors.texto = "El texto debe ser un string";
    }
    return errors;
};

export default (state = initialState, action) => {

    switch (action.type) {
        case SEND_TEXT_SUCCESS:
            {
                const texto = action.payload;
                const errors = setErrors(texto);
                if (errors.texto) {
                    const values = {
                        texto: ""
                    };
                    return {
                        textForm: {
                            values,
                            errors
                        }
                    };
                } else {
                    const values = {
                        texto: action.payload
                    };
                    return {
                        textForm: {
                            values,
                            errors
                        }
                    };
                }
            }
        case SEND_TEXT_REQUEST:
            return {
                ...state
            }
        case SEND_TEXT_FAILURE:
            const errors = { texto: action.payload }
            const values = {
                texto: ""
            };
            return {
                textForm: {
                    values,
                    errors
                }
            }
        default:
            return state;
    }
};