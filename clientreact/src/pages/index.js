import React from 'react'
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import reducer from "../reducers/formReducer";
import TextForm from '../components/form';

const middlewares = [thunk];

if (process.env.NODE_ENV === `development`) {
   const { logger } = require(`redux-logger`);

   middlewares.push(logger);
}

const store = createStore(reducer, applyMiddleware(...middlewares));

export const Index = () => (
    <Provider store={store}>
    	<div>
			<TextForm/>
		</div>
    </Provider>
)