import React from 'react'

export const Acerca = () => (
	<div>
		<h2>ACERCA</h2>
		<hr/>
		<p>La presente aplicación fue desarrollada usando Node JS 8 y Express para la parte de servidor</p>
		<p>Para el cliente se utilizo React, Redux, Bootstrap y Thonk para las llamadas asincronas</p>
	</div>
)