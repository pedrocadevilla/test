import React from 'react';
import { Jumbotron as Jumbo, Container } from 'react-bootstrap';
import styled from 'styled-components';
import toolboxImage from '../images/toolbox.jpg'

const Styles = styled.div `
	.jumbo {
		background: url(${toolboxImage});
		background-repeat: no-repeat;
  		background-size: contain;
  		background-position: 100% 0;
		color: #ccc;
		height: 200px;
		position: relative;
		z-index: -2;
	}

	.overlay{
		background-color: #7A5959;
		opacity: 0.6;
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		z-index: -1;
	}

	h1, p{
		color: black;
	}
`;

export const Jumbotron = () => (
	<Styles>
		<Jumbo fluid className="jumbo">
			<div className="overlay"></div>
			<Container>
				<h1>React Rest Api</h1>
				<p>El siguiente es un cliente Rest usando React, Redux y bootstrap.</p>
			</Container>
		</Jumbo>
	</Styles>
)