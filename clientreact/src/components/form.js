import React, { useState } from 'react';
import { Form , Alert } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { connect } from "react-redux";
import styled from 'styled-components';
import * as formActions from '../actions/formAction';

const Styles = styled.div `
  button{
    background-color: #870707;
    border: #870707;
    &:hover{
      background-color: #E32626;
      border: #E32626;
    }
    &:focus{
      background-color: #E32626;
      border: #E32626;
      outline: none !important;
      box-shadow: none;
    }
  }
`;

const TextForm = (props) => {
  const [texto, setTexto] = useState("");

  return(
    <Styles>
      <Form>
        <h2>Texto a enviar</h2>
        <hr/>
        <Form.Group controlId="formBasicText">
          <Form.Control type="text" placeholder="Introduzca el texto" 
          isInvalid={props.textForm.errors.texto.length > 0}
          isValid={
            props.textForm.values.texto &&
            props.textForm.errors.texto.length === 0
          }
          onChange={e => setTexto(e.target.value)}/>
          <Form.Control.Feedback type="invalid">{props.textForm.errors.texto}</Form.Control.Feedback>
          <Form.Text className="text-muted">
            El texto introducido sera el mismo que retornara la aplicacion.
          </Form.Text>
        </Form.Group>
        <Button variant="primary" type="button"
        onClick={props.setActualText(texto)}>
          Enviar
        </Button>
        <hr/>
        <Alert variant="success" show={props.textForm.values.texto !== ""}>
          El texto escrito es {props.textForm.values.texto}
        </Alert>
      </Form>
    </Styles>
  )
}

const mapStateToProps = state => ({
  textForm: state.textForm
});

const mapDispatchToProps = dispatch => ({
  setActualText(texto) {
    return () => {
      dispatch(formActions.setCurrentText(texto));
    };
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TextForm);